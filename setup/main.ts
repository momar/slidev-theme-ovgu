import { defineAppSetup } from '@slidev/types'

export default defineAppSetup(({ app, router }) => {
    // Vue App
    app.config.globalProperties.$ovgu = {
        "faculties": {
            "none": {
                "color": "#7a003f",
            },
            "mb": {
                "color": "#009ee3",
            },
            "vst": {
                "color": "#941680",
            },
            "eit": {
                "color": "#94c12d",
            },
            "inf": {
                "color": "#0068b4",
            },
            "math": {
                "color": "#d13f58",
            },
            "nat": {
                "color": "#05a535",
            },
            "med": {
                "color": "#012d5e",
            },
            "hw": {
                "color": "#f39100",
            },
            "ww": {
                "color": "#5d8ea6",
            },
        },
    };
})
