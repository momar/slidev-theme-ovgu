---
theme: ./
themeConfig:
  faculty: none # possible values: none|mb|vst|eit|inf|math|nat|med|hw|ww
  logoTitle: # "Fakultät für\nInformatik"
layout: title
---

# Slidev Example Presentation
## Get familiar with the possibilities of Slidev and this template

Presentation slides for developers

---
layout: title2
---

# Slidev Example Presentation
## Get familiar with the possibilities of Slidev and this template

Presentation slides for developers  
(this slide is using an alternative title layout)

---

# What is Slidev?

> [Slidev](https://sli.dev/guide/why) is a slides maker and presenter designed for developers, consist of the following features

- 📝 **Text-based** - focus on the content with Markdown, and then style them later
- 🎨 **Themable** - theme can be shared and used with npm packages
- 🧑‍💻 **Developer Friendly** - code highlighting, live coding with autocompletion
- 🤹 **Interactive** - embedding Vue components to enhance your expressions
- 🎥 **Recording** - built-in recording and camera view
- 📤 **Portable** - export into PDF, PNGs, or even a hostable SPA
- 🛠 **Hackable** - anything possible on a webpage

---

# Navigation

Hover on the bottom-left corner to see the navigation's controls panel

### Keyboard Shortcuts

|     |     |
| --- | --- |
| <kbd>space</kbd> / <kbd>tab</kbd> / <kbd>right</kbd> | next animation or slide |
| <kbd>left</kbd>  / <kbd>shift</kbd><kbd>space</kbd> | previous animation or slide |
| <kbd>up</kbd> | previous slide |
| <kbd>down</kbd> | next slide |

---

# Code

![](https://source.unsplash.com/collection/94734566/1920x150#wide)

<div class="right">

```ts
interface User {
  id: number
  firstName: string
  lastName: string
  role: string
}
function updateUser(id: number, update: Partial<User>) {
  const user = getUser(id)
  const newUser = {...user, ...update}  
  saveUser(id, newUser)
}
```

</div>

Use code snippets and get the highlighting directly!

Flights are ghostly airships. Framed in a different way, we can assume that any instance of a korean can be construed as a truer recorder. Some posit the ranking trade to be less than peaky. A dinner is a brashy latency.

---

# Cool helpers

<figure class="left">
<img src="https://source.unsplash.com/collection/94734566/600x800" width="200">
<figcaption><count-ref id="test" scope="picture">Abb. #:</count-ref>
Example Picture
</figcaption></figure>

You can use awesome helper classes like the following to align text & images:

- `<div class="left">...</div>` or `![](...#left)`  
  to align something to the left
- `<div class="right">...</div>` or `![](...#right)`  
  to align something to the right
- `<div class="center">...</div>` or `![](...#center)`  
  to center something (if there's nothing else on the page, it'll be vertically centered as well)
- `<div class="wide">...</div>` or `![](...#wide)`  
  to fill the whole slide width

---

<div class="center">

# Learn More

[Documentations](https://sli.dev) / [GitHub Repo](https://github.com/slidevjs/slidev)

</div>

---
class: with-background
---

<div class="absolute bottom-0 left-0 p-10 py-20">

# Vielen Dank für Ihre Aufmerksamkeit!

Mehr Infos auf [go.momar.de/example]()

</div>
